package com.company;

public class BlackEdition extends Decorator{

    public BlackEdition(Phone newP1) {
        super(newP1);
        System.out.println("Coloring to black");
    }

    public String getInfo(){
        return p1.getInfo() + ", 256gb";
    }

    public double getPrice(){
        return p1.getPrice() + 200;
    }
}
