package com.company;

public class ICloud extends Decorator {
    public ICloud(Phone newP1) {
        super(newP1);
        System.out.println("Adding additional space in ICloud");
    }

    public String getInfo(){
        return p1.getInfo() + ", ICloud";
    }

    public double getPrice(){
        return p1.getPrice() + 100;
    }
}
