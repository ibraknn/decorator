package com.company;

public class Decorator implements Phone{

    protected Phone p1;

    public Decorator(Phone newP1){
        p1 = newP1;
    }

    public String getInfo(){
        return p1.getInfo();
    }

    public double getPrice(){
        return p1.getPrice();
    }
}
