package com.company;

public interface Phone {

    public abstract String getInfo();

    public abstract double getPrice();

}
