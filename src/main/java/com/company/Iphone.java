package com.company;

public class Iphone implements Phone {


    @Override
    public String getInfo() {
        return "Iphone 11 pro max";
    }

    @Override
    public double getPrice() {
        return 1200.00;
    }
}
