package com.company;

public class Main {

    public static void main(String[] args) {
        Phone p1 = new BlackEdition(new ICloud(new Iphone()));

        System.out.println("Info: " + p1.getInfo());

        System.out.println("Price: " + p1.getPrice());
    }
}
